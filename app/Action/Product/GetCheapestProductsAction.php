<?php

declare(strict_types=1);

namespace App\Action\Product;

class GetCheapestProductsAction
{
    
    public function execute(): GetCheapestProductsResponse
    {
        return new GetCheapestProductsResponse();
    }
}