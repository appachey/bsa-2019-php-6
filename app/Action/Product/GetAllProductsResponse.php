<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetAllProductsResponse
{

    public function getProducts(): array
    {
        $products = app()->make(ProductRepositoryInterface::class);
        return $products->findAll();
    }
}