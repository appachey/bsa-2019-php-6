<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetCheapestProductsResponse
{
    public function getProducts(): array
    {
        $products = app()->make(ProductRepositoryInterface::class);
        $allProducts = $products->findAll();
        usort($allProducts, function($a, $b){
            return $a->getPrice() <=> $b->getPrice();
        });
        return array_slice($allProducts, 0, 3);
    }
}