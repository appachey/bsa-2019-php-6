<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;
use App\Repository\ProductRepositoryInterface;

class GetMostPopularProductResponse
{
    public function getProduct(): Product
    {
        $productsRepo = app()->make(ProductRepositoryInterface::class);
        $products = $productsRepo->findAll();
        usort($products, function($a, $b){
            return $a->getRating() <=> $b->getRating();
        });
        return $products[array_key_last($products)];
    }
}