<?php

use App\Repository\ProductRepositoryInterface;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/products', function(){
    $allProductsResp = (new \App\Action\Product\GetAllProductsAction())->execute();
    $collection = \App\Http\Presenter\ProductArrayPresenter::presentCollection($allProductsResp->getProducts());
    return response(json_encode($collection))->header('Content-Type', 'application/json');
});

Route::get('/products/popular', function(){
    $allProductsResp = (new \App\Action\Product\GetMostPopularProductAction())->execute();
    $product = \App\Http\Presenter\ProductArrayPresenter::present($allProductsResp->getProduct());
    return response(json_encode($product))->header('Content-Type', 'application/json');
});