<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/products/cheap', function(){
    $allProductsResp = (new \App\Action\Product\GetCheapestProductsAction())->execute();
    $products = \App\Http\Presenter\ProductArrayPresenter::presentCollection($allProductsResp->getProducts());
    //return Response::view('cheap_products', ['products' => $products]);
    return view('cheap_products', compact('products'))->render();
});
