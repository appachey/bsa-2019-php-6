<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Cheap products</title>

    <!-- Styles -->
    <style>

    </style>
</head>
<body>
    List of cheap products
    @foreach ($products as $value)
        <p><b>Id:</b> {{$value['id']}}</p>
        <p><b>Name</b>: {{$value['name']}}</p>
        <p><b>Price:</b> {{$value['price']}}</p>
        <p><b>Img URL:</b> {{$value['img']}}</p>
        <p><b>Rating:</b> {{$value['rating']}}</p>
        <hr/>
    @endforeach
</body>
</html>